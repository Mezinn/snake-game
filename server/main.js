const server = require('http').createServer();
const io = require('socket.io')(server);

class Window {
    constructor(width, height) {
        this.width = width;
        this.height = height;
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    getPoint(color) {
        return new Point(Math.floor(Math.random() * this.width), Math.floor(Math.random() * this.height), color)
    }
}

class Point {
    constructor(x, y, color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }

    getX() {
        return this.x;
    }

    getY() {
        return this.y;
    }

    add(another) {
        return new Point(this.x + another.x, this.y + another.y, this.color);
    }

    isEqual(another) {
        return this.x === another.x && this.y === another.y;
    }
}

class Snake {

    constructor(begin) {
        this.body = [begin];
    }

    setDirection(dir) {

        if (dir in DIRECTIONS) {
            this.direction = DIRECTIONS[dir];
        }

        return this;
    }

    getDirection() {
        return this.direction;
    }

    getHead() {
        return this.body[0];
    }

    getBody() {
        return this.body;
    }

    move() {

        let head = this.getHead().add(this.getDirection());

        if (head.getX() > window.getWidth()) {
            head = new Point(0, head.getY(), 'black');
        }

        if (head.getX() < 0) {
            head = new Point(window.getWidth(), head.getY(), 'black');
        }

        if (head.getY() > window.getHeight()) {
            head = new Point(head.getX(), 0, 'black');
        }

        if (head.getY() < 0) {
            head = new Point(head.getX(), window.getHeight(), 'black');
        }

        this.body.unshift(head);

        if(head.isEqual(cherry)){
            cherry = window.getPoint('red');
        }else{
            this.body.pop();
        }
    }

    setName(name) {
        this.name = name;
    }

    getName() {
        return this.name;
    }
}

class Timer {

    constructor(ms) {
        this.ms = ms;
    }

    tick(func) {
        this.func = func;
    }

    start() {
        this.id = setInterval(this.func, this.ms);
    }

    stop() {
        clearInterval(this.id);
    }
}

const DIRECTIONS = {
    'ArrowUp': new Point(0, -1),
    'ArrowDown': new Point(0, 1),
    'ArrowLeft': new Point(-1, 0),
    'ArrowRight': new Point(1, 0),
};

let window = new Window(29, 29);

let snakes = new Map();

let cherry = window.getPoint('red');
let bounty = window.getPoint('cyan');

const timer = new Timer(300);


timer.tick(function () {

    let snakesArray = [];

    snakes.forEach(function (snake) {
        snakesArray.push(snake);
    });

    snakes.forEach(function (snake, socket) {
        snake.move();
        socket.emit('sync', {
            snakes: snakesArray,
            cherry,
            bounty,
            window
        });
    });
});


io.on('connection', function (socket) {


    socket.on('disconnect',function(){
       snakes.delete(socket);
    });

    let snake = null;

    if (!snakes.has(socket)) {
        snake = new Snake(window.getPoint('black'));
        snakes.set(socket, snake);
        snake.setDirection('ArrowDown');
    } else {
        snake = snakes.get(socket);
    }



    socket.on('keyDown', function ({key}) {
        snake.setDirection(key);
    });

});

server.listen(3000);

timer.start();






