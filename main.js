class Point {
    constructor(x, y, color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }

    getX() {
        return this.x;
    }

    getY() {
        return this.y;
    }

    getColor() {
        return this.color;
    }
}

class Window {

    constructor(config) {
        this.pointSize = 25;
        this.size = 30;
        this.$container = null;
        this.keyDownHandler = null;
        Object.assign(this, config);

        if (this.$container === null) {
            throw new Error('Container must be set.');
        }

        this.$container.style.width = `${this.pointSize * this.size}px`;
        this.$container.style.height = `${this.pointSize * this.size}px`;

        let rect = this.$container.getBoundingClientRect();
        this.offsetX = rect.x;
        this.offsetY = rect.y;

        document.addEventListener('keydown', function (event) {
            if (this.keyDownHandler !== null) {
                this.keyDownHandler(event);
            }
        }.bind(this));


    }

    onKeyDown(func) {
        this.keyDownHandler = func;
    }

    draw(points) {

        this.$container.innerHTML = '';
        for (let point of points) {
            let $point = document.createElement('div');
            $point.style.position = 'absolute';
            $point.style.width = `${this.pointSize}px`;
            $point.style.height = `${this.pointSize}px`;
            $point.style.backgroundColor = point.getColor();
            $point.style.top = `${this.offsetY + (point.getY() * this.pointSize)}px`;
            $point.style.left = `${this.offsetX + (point.getX() * this.pointSize)}px`;
            this.$container.append($point);
        }
    }
}

class Client {

    constructor(socket, window) {
        this.socket = socket;
        this.window = window;
    }

    handle() {



        this.socket.on('sync', function (data) {

            const points = data.snakes.reduce(function (points, snake) {
                snake.body.forEach(function (point) {
                    points.push(new Point(point.x, point.y, point.color));
                });
                return points;
            }, []);

            const bounty = data.bounty;

            points.push(new Point(bounty.x, bounty.y, bounty.color));

            const cherry = data.cherry;

            points.push(new Point(cherry.x, cherry.y, cherry.color));


            this.window.draw(points);
        }.bind(this));

        this.window.onKeyDown(function (event) {
            this.socket.emit('keyDown', {key: event.key});
        }.bind(this));
    }
}

const client = new Client(
    io('http://localhost:3000'),
    new Window({
        $container: document.getElementById('field'),
    })
);


client.handle();
